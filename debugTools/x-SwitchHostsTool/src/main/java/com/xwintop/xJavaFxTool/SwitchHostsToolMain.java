package com.xwintop.xJavaFxTool;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SwitchHostsToolMain extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fXMLLoader = SwitchHostsToolMain.getFXMLLoader();
        ResourceBundle resourceBundle = fXMLLoader.getResources();
        Parent root = fXMLLoader.load();
        primaryStage.setResizable(true);
        primaryStage.setTitle(resourceBundle.getString("Title"));
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static FXMLLoader getFXMLLoader() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("locale.SwitchHostsTool");
        URL url = Object.class.getResource("/com/xwintop/xJavaFxTool/fxmlView/debugTools/SwitchHostsTool.fxml");
        return new FXMLLoader(url, resourceBundle);
    }
}