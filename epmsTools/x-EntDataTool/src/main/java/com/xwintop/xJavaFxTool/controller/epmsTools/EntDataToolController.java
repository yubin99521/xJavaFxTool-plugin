package com.xwintop.xJavaFxTool.controller.epmsTools;

import com.xwintop.xJavaFxTool.services.epmsTools.EntDataToolService;
import com.xwintop.xJavaFxTool.view.epmsTools.EntDataToolView;
import com.xwintop.xcore.util.javafx.JavaFxViewUtil;
import com.xwintop.xcore.util.javafx.TooltipUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.cell.CheckBoxTreeCell;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.net.URL;
import java.util.ResourceBundle;

@Getter
@Setter
@Slf4j
public class EntDataToolController extends EntDataToolView {
    private EntDataToolService entDataToolService = new EntDataToolService(this);

    private String[] dbTypeStrings = new String[]{"mysql", "sqlserver", "oracle"};

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initView();
        initEvent();
        initService();
    }

    private void initView() {
        hostText1.setText("192.168.129.121");
        dbNameText1.setText("test");
        pwdText1.setText("easipass");

        JavaFxViewUtil.setPasswordTextFieldFactory(pwdText1);
        JavaFxViewUtil.setPasswordTextFieldFactory(pwdText2);
        dbTypeText1.getItems().addAll(dbTypeStrings);
        dbTypeText1.setValue(dbTypeStrings[0]);
        dbTypeText2.getItems().addAll(dbTypeStrings);
        dbTypeText2.setValue(dbTypeStrings[0]);

        CheckBoxTreeItem<String> rootItem = new CheckBoxTreeItem<>("源端库表");
        rootItem.setExpanded(true);
        tableTreeView1.setCellFactory(CheckBoxTreeCell.<String>forTreeView());
        tableTreeView1.setRoot(rootItem);
        tableTreeView1.setShowRoot(true);
        tableTreeView2.setCellFactory(CheckBoxTreeCell.<String>forTreeView());
        tableTreeView2.setRoot(new CheckBoxTreeItem<>("目标端库表"));
        tableTreeView2.getRoot().setExpanded(true);
        tableTreeView2.setShowRoot(true);
        JavaFxViewUtil.setSpinnerValueFactory(channelSpinner, 1, Integer.MAX_VALUE, 6);
    }

    private void initEvent() {
    }

    private void initService() {
    }

    @FXML
    private void connectAction1(ActionEvent event) {
        try {
            entDataToolService.connectAction(dbTypeText1.getValue(), hostText1.getText(), portText1.getText(), dbNameText1.getText(), userNameText1.getText(), pwdText1.getText(), tableTreeView1);
        } catch (Exception e) {
            log.error("连接失败：", e);
            TooltipUtil.showToast("连接失败：" + e.getMessage());
        }
    }

    @FXML
    private void connectAction2(ActionEvent event) {
        try {
            entDataToolService.connectAction(dbTypeText2.getValue(), hostText2.getText(), portText2.getText(), dbNameText2.getText(), userNameText2.getText(), pwdText2.getText(), tableTreeView2);
        } catch (Exception e) {
            log.error("连接失败：", e);
            TooltipUtil.showToast("连接失败：" + e.getMessage());
        }
    }

    @FXML
    private void buildCloneAction(ActionEvent event) {
        try {
            entDataToolService.buildCloneAction();
        } catch (Exception e) {
            log.error("生成失败：", e);
            TooltipUtil.showToast("生成失败：" + e.getMessage());
        }
    }
    @FXML
    private void buildStandardAction(ActionEvent event) {
        TooltipUtil.showToast("该功能未开发");
    }
}
