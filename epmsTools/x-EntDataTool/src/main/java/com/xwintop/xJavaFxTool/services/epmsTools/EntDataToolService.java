package com.xwintop.xJavaFxTool.services.epmsTools;

import cn.hutool.db.meta.Column;
import cn.hutool.db.meta.MetaUtil;
import cn.hutool.db.meta.Table;
import cn.hutool.db.meta.TableType;
import com.alibaba.druid.filter.config.ConfigTools;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xwintop.xJavaFxTool.controller.epmsTools.EntDataToolController;
import com.xwintop.xJavaFxTool.model.TaskConfigClient;
import com.xwintop.xJavaFxTool.tools.DataxJsonUtil;
import com.xwintop.xcore.util.javafx.TooltipUtil;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.HBox;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Slf4j
public class EntDataToolService {
    private EntDataToolController entDataToolController;

    public EntDataToolService(EntDataToolController entDataToolController) {
        this.entDataToolController = entDataToolController;
    }

    //连接数据库
    public void connectAction(String dbType, String dbIp, String dbPort, String dbName, String dbUserName, String dbUserPassword, TreeView<String> tableTreeView) throws Exception {
        DruidDataSource dataSource = new DruidDataSource();
        try {
            dataSource.setUrl(DataxJsonUtil.getJdbcUrl(dbType, dbIp, dbPort, dbName));
            dataSource.setUsername(dbUserName);
            dataSource.setPassword(dbUserPassword);
            dataSource.setFailFast(true);
            dataSource.init();
            List<String> tableNames = MetaUtil.getTables(dataSource, DataxJsonUtil.convertDatabaseCharsetType(dbUserName, dbType), TableType.TABLE);
            log.info("获取到表名:" + tableNames);
            CheckBoxTreeItem<String> treeItem = (CheckBoxTreeItem<String>) tableTreeView.getRoot();
            treeItem.getChildren().clear();
            for (String tableName : tableNames) {
                final CheckBoxTreeItem<String> tableNameTreeItem = new CheckBoxTreeItem<>(tableName);
                treeItem.getChildren().add(tableNameTreeItem);
                Table table = MetaUtil.getTableMeta(dataSource, tableName);
                for (Column column : table.getColumns()) {
                    CheckBox isPkCheckBox = new CheckBox("Pk");
                    if (table.getPkNames().size() == 1) {
                        if (column.getName().equals(table.getPkNames().toArray()[0])) {
                            isPkCheckBox.setSelected(true);
                        }
                    }
                    CheckBox isTimeCheckBox = new CheckBox("Time");
                    HBox hBox = new HBox();
                    hBox.getChildren().addAll(isPkCheckBox, isTimeCheckBox);
                    final CheckBoxTreeItem<String> columnNameTreeItem = new CheckBoxTreeItem<>(column.getName(), hBox);
                    tableNameTreeItem.getChildren().add(columnNameTreeItem);
                }
//            String[] columnNames = MetaUtil.getColumnNames(dataSource, tableName);
//            for (String columnName : columnNames) {
//                CheckBox isPkCheckBox = new CheckBox("Pk");
//                CheckBox isTimeCheckBox = new CheckBox("Time");
//                HBox hBox = new HBox();
//                hBox.getChildren().addAll(isPkCheckBox, isTimeCheckBox);
//                final CheckBoxTreeItem<String> columnNameTreeItem = new CheckBoxTreeItem<>(columnName, hBox);
//                tableNameTreeItem.getChildren().add(columnNameTreeItem);
//            }
            }
        } finally {
            if (dataSource != null) {
                dataSource.close();
            }
        }
    }

    public void buildCloneAction() throws Exception {
        List<TreeItem<String>> rootList = new ArrayList<>();
        rootList.addAll(entDataToolController.getTableTreeView1().getRoot().getChildren());
        rootList.addAll(entDataToolController.getTableTreeView2().getRoot().getChildren());
        if (rootList.isEmpty()) {
            TooltipUtil.showToast("未勾选表！");
        }
        for (TreeItem<String> treeItem : rootList) {
            CheckBoxTreeItem<String> tableNameTreeItem = (CheckBoxTreeItem<String>) treeItem;
            if (tableNameTreeItem.isSelected()) {
                List columnList = new ArrayList();
                String splitPk = "";
                String where = "";
                for (TreeItem<String> childTreeItem : tableNameTreeItem.getChildren()) {
                    CheckBoxTreeItem<String> columnNameTreeItem = (CheckBoxTreeItem<String>) childTreeItem;
                    HBox hBox = (HBox) columnNameTreeItem.getGraphic();
                    CheckBox isPkCheckBox = (CheckBox) hBox.getChildren().get(0);
                    CheckBox isTimeCheckBox = (CheckBox) hBox.getChildren().get(1);
                    if (isPkCheckBox.isSelected()) {
                        splitPk = columnNameTreeItem.getValue();
                    }
                    if (isTimeCheckBox.isSelected()) {
                        where = columnNameTreeItem.getValue();
                    }
                    if (columnNameTreeItem.isSelected()) {
                        columnList.add(columnNameTreeItem.getValue());
                    }
                }
                buildCloneJson(tableNameTreeItem.getValue(), (String[]) columnList.toArray(new String[0]), splitPk, where);
            }
        }
    }

    public void buildCloneJson(String tableName, String[] columnStrings, String splitPk, String where) throws Exception {
        String[] arr = ConfigTools.genKeyPair(512);
        JSONObject jsonObject = getReaderConfig(tableName, columnStrings, splitPk, where);
        TaskConfigClient taskConfigClient = new TaskConfigClient();
        taskConfigClient.setName(entDataToolController.getCompanyNameTextField().getText() + "_" + tableName + "_2clone.json");
        taskConfigClient.setIsEnable(true);
        taskConfigClient.setExecuteTimes(0);
        taskConfigClient.setTriggerType("SIMPLE");
        taskConfigClient.setTriggerCron("0 0 1/23 * * ? *");
        taskConfigClient.setIsStatefulJob(true);
        taskConfigClient.setJobJson(jsonObject.toJSONString());
        if (StringUtils.isEmpty(where)) {
            taskConfigClient.setLastSyncTime(null);
        } else {
            taskConfigClient.setLastSyncTime(0L);
        }
        taskConfigClient.setDecrypt(entDataToolController.getPwdText1().isVisible());
        if (taskConfigClient.isDecrypt()) {
            taskConfigClient.setPublicKey(arr[1]);
            taskConfigClient.setPassword(ConfigTools.encrypt(arr[0], entDataToolController.getPwdText1().getText()));
        } else {
            taskConfigClient.setPassword(entDataToolController.getPwdText1().getText());
            taskConfigClient.setPublicKey("");
        }
        taskConfigClient.setTdecrypt(entDataToolController.getPwdText2().isVisible());
        if (taskConfigClient.isTdecrypt()) {
            taskConfigClient.setTpassword(ConfigTools.encrypt(arr[0], entDataToolController.getPwdText2().getText()));
            taskConfigClient.setTpublicKey(arr[1]);
        } else {
            taskConfigClient.setTpassword(entDataToolController.getPwdText2().getText());
            taskConfigClient.setTpublicKey("");
        }
        File jsonFile = new File("./executor", entDataToolController.getCompanyNameTextField().getText() + "_" + tableName + "_2clone.json");
        FileUtils.writeStringToFile(jsonFile, JSON.toJSONString(taskConfigClient), "utf-8");
        log.info("生成成功:" + jsonFile.getCanonicalPath());
        TooltipUtil.showToast("生成成功:" + jsonFile.getCanonicalPath());
    }

    public JSONObject getReaderConfig(String tableName, String[] columnStrings, String splitPk, String where) {
        JSONObject jsonObject = new JSONObject();
        JSONObject jobJson = new JSONObject();
        JSONObject settingJson = new JSONObject();
        JSONObject speedJson = new JSONObject();
        speedJson.put("channel", entDataToolController.getChannelSpinner().getValue());
        settingJson.put("speed", speedJson);
        jobJson.put("setting", settingJson);

        JSONArray contentJsonArray = new JSONArray();
        JSONObject contentJson = new JSONObject();
        JSONObject readerJson = new JSONObject();
        readerJson.put("name", DataxJsonUtil.getDataxReaderName(entDataToolController.getDbTypeText1().getValue()));
        JSONObject parameterJson = new JSONObject();
        parameterJson.put("username", entDataToolController.getUserNameText1().getText());
        parameterJson.put("password", "${spwd}");
        parameterJson.put("where", where);
        parameterJson.put("splitPk", splitPk);
        JSONArray connectionJsonArrayReader = new JSONArray();

        parameterJson.put("column", columnStrings);
        JSONObject connectionJsonReader = new JSONObject();
        connectionJsonReader.put("table", new String[]{tableName});
        String jdbcUrlReader = DataxJsonUtil.getJdbcUrl(entDataToolController.getDbTypeText1().getValue(), entDataToolController.getHostText1().getText(), entDataToolController.getPortText1().getText(), entDataToolController.getDbNameText1().getText());
        connectionJsonReader.put("jdbcUrl", new String[]{jdbcUrlReader});
        connectionJsonArrayReader.add(connectionJsonReader);
        parameterJson.put("connection", connectionJsonArrayReader);
        readerJson.put("parameter", parameterJson);

        contentJson.put("reader", readerJson);

        JSONObject writerJson = new JSONObject();
        writerJson.put("name", DataxJsonUtil.getDataxWriterName(entDataToolController.getDbTypeText2().getValue()));
        JSONObject parameterJsonWriter = new JSONObject();
        parameterJsonWriter.put("username", entDataToolController.getUserNameText2().getText());
        parameterJsonWriter.put("password", "${tpwd}");

        parameterJsonWriter.put("column", columnStrings);

        JSONArray connectionJsonArrayWriter = new JSONArray();
        JSONObject connectionJsonWriter = new JSONObject();

        String jdbcUrlWriter = DataxJsonUtil.getJdbcUrl(entDataToolController.getDbTypeText2().getValue(), entDataToolController.getHostText2().getText(), entDataToolController.getPortText2().getText(), entDataToolController.getDbNameText2().getText());
        connectionJsonWriter.put("jdbcUrl", jdbcUrlWriter);
        connectionJsonWriter.put("table", new String[]{tableName});
        connectionJsonArrayWriter.add(connectionJsonWriter);
        parameterJsonWriter.put("connection", connectionJsonArrayWriter);
        writerJson.put("parameter", parameterJsonWriter);

        contentJson.put("writer", writerJson);
        contentJsonArray.add(contentJson);
        jobJson.put("content", contentJsonArray);

        jsonObject.put("job", jobJson);
        return jsonObject;
    }
}
