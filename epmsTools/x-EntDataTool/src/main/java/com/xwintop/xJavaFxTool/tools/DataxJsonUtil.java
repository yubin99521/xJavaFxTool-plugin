package com.xwintop.xJavaFxTool.tools;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.sql.Timestamp;

public class DataxJsonUtil {
    public static String getDataxReaderName(String DB_TYPE) {
        if ("sqlserver".equalsIgnoreCase(DB_TYPE)) {
            return "sqlserverreader";
        } else if ("oracle".equalsIgnoreCase(DB_TYPE)) {
            return "oraclereader";
        } else if ("mysql".equalsIgnoreCase(DB_TYPE)) {
            return "mysqlreader";
        }
        return DB_TYPE;
    }

    public static String getDataxWriterName(String DB_TYPE) {
        if ("sqlserver".equalsIgnoreCase(DB_TYPE)) {
            return "sqlserverwriter";
        } else if ("oracle".equalsIgnoreCase(DB_TYPE)) {
            return "oraclewriter";
        } else if ("mysql".equalsIgnoreCase(DB_TYPE)) {
            return "mysqlwriter";
        }
        return DB_TYPE;
    }

    public static String getDataxWhereSql(String DB_TYPE, String filterTimeColumn, Timestamp lastSyncTime) {
        if ("sqlserver".equalsIgnoreCase(DB_TYPE)) {
            return filterTimeColumn + " > CONVERT(datetime,'" + DateFormatUtils.format(lastSyncTime, "yyyy-MM-dd HH:mm:ss") + "',20)";
        } else if ("oracle".equalsIgnoreCase(DB_TYPE)) {
//            return filterTimeColumn + " > TO_TIMESTAMP('${LastSyncTime}','yyyy-MM-dd-hh24:mi:ss.ff6')";
            return filterTimeColumn + " > TO_TIMESTAMP('" + DateFormatUtils.format(lastSyncTime, "yyyy-MM-dd-HH:mm:ss") + String.format(".%06d", lastSyncTime.getNanos() / 1000) + "','yyyy-MM-dd-hh24:mi:ss.ff6')";
        } else if ("mysql".equalsIgnoreCase(DB_TYPE)) {
            return filterTimeColumn + " > str_to_date('" + DateFormatUtils.format(lastSyncTime, "yyyy-MM-dd HH:mm:ss") + "','%Y-%m-%d %H:%i:%s')";
        }
        return DB_TYPE;
    }

    /**
     * 获取jdbc连接地址
     */
    public static String getJdbcUrl(String DB_TYPE, String dbIp, String dbPort, String dbName) {
        String jdbcUrl = null;
        if ("sqlserver".equalsIgnoreCase(DB_TYPE)) {
            //jdbc:sqlserver://localhost:3433;DatabaseName=dbname
            jdbcUrl = "jdbc:sqlserver://" + dbIp + ":" + dbPort + ";DatabaseName=" + dbName;
        } else if ("oracle".equalsIgnoreCase(DB_TYPE)) {
            //jdbc:oracle:thin:@[HOST_NAME]:PORT:[DATABASE_NAME]
            jdbcUrl = "jdbc:oracle:thin:@" + dbIp + ":" + dbPort + "/" + dbName;
        } else if ("mysql".equalsIgnoreCase(DB_TYPE)) {
            //jdbc:mysql://bad_ip:3306/database
            jdbcUrl = "jdbc:mysql://" + dbIp + ":" + dbPort + "/" + dbName;
        }
        return jdbcUrl;
    }

    public static String convertDatabaseCharsetType(String in, String type) {
        String dbUser;
        if (in != null) {
            if (type.equals("oracle")) {
                dbUser = in.toUpperCase();
            } else if (type.equals("postgresql")) {
                dbUser = "public";
            } else if (type.equals("mysql")) {
                dbUser = null;
            } else if (type.equals("mssqlserver")) {
                dbUser = null;
            } else if (type.equals("db2")) {
                dbUser = in.toUpperCase();
            } else {
                dbUser = in;
            }
        } else {
            dbUser = "public";
        }
        return dbUser;
    }
}
